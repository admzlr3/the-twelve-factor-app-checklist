# Twelve Factor nodejs app

This checklist aims to captures best practices around the [twelve-factor methodology](https://12factor.net)  and how this is implemented for the services developed for P&P microservices. Implementing project needs to include the copy of this into their README and populate the How we do it part before deploying the services into production.


## The Twelve Factors


### [I. Codebase](https://12factor.net/codebase)

One codebase tracked in revision control, many deploys.


#### How we do it


[comment]: # (include notes on the CD scripts)


### [II. Dependencies](https://12factor.net/dependencies)

Explicitly declare and isolate dependencies.


#### How we do it




### [III. Config](https://12factor.net/config)

Store config in the environment.


#### How we do it




### [IV. Backing services](https://12factor.net/backing-services)

Treat backing services as attached resources.


#### How we do it




### [V. Build, release, run](https://12factor.net/build-release-run)

Strictly separate build and run stages.


#### How we do it



### [VI. Processes](https://12factor.net/processes)

Execute the app as one or more stateless processes.


#### How we do it




### [VII. Port binding](https://12factor.net/port-binding)

Export services via port binding.


#### How we do it




### [VIII. Concurrency](https://12factor.net/concurrency)

Scale out via the process model


#### How we do it



### [XI. Logs](https://12factor.net/logs)

Treat logs as event streams.


#### How we do it




### [XII. Admin processes](https://12factor.net/admin-processes)

Run admin/management tasks as one-off processes.


#### How we do it




## Beyond Twelve Factor identify any best practices followed for e.g.


### Blue/green deploy

#### How we do it


### Continuous Integration and Delivery with Git Lab

Git Lab is a useful tool for managing relases using git branches. The idea
is that branches map to different deployment environments.

`master` represents well-vetted and partner accepted work and deploys to the
production environment.

`release-x.y` is on track for release and deploys to your staging environment.
This allows partners to review the work before it is released. Each release
branch is based on development. Once created, this allows developers to commit
bug fixes directly to the release branch without hindering development on the
next release.

`development` is an integration branch. This allows developers on your team
to test their work-in-progress features with features from other developers.

`feature-\*` branches represent a single feature. One or more developers may be
committing to this branch. When the feature is tested and reviewed it can be
merged to `development`.
